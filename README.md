## Product Catalog App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description

Application to menage products in catalog.

## Used Technologies and Patterns:

- React
- Redux
- Hooks
- Axios
- Material Ui
- Type Script
- Prettier
- Atomic Desing
- RWD
- Tests
- Storybook

## Functionalities

- Fetching products from API
- Searching products by text, active and promo flag
- Displaying products details in portal modal
- Pagination

## Quick Start

To start the development:

```
mkdir new-project
cd new-project/
git clone git@github.com:TheSoftwareHouse/react-interview-starter-ts.git .
rm -r .git
npm install
cd e2e
npm install
cd ..
cp .env.dist .env
cp .env.e2e.dist ./e2e/.env
```
