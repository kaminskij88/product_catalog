import { Story, Meta } from "@storybook/react";
import Spinner, { SpinnerProps } from "../../app/components/atoms/Spinner";
import React from "react";

export default {
  title: "Atom/Spinner",
  component: Spinner,
  argTypes: {
    onChange: { action: "change" },
  },
} as Meta;

const Template: Story<SpinnerProps> = (args) => <Spinner {...args} />;

export const LoadingSpinner = Template.bind({});

LoadingSpinner.args = {
  size: 10,
};
