import { Story, Meta } from "@storybook/react";
import Checkbox, { CheckboxProps } from "../../app/components/atoms/Checkbox";
import React from "react";

export default {
  title: "Atom/Checkbox",
  component: Checkbox,
  argTypes: {
    onChange: { action: "change" },
  },
} as Meta;

const Template: Story<CheckboxProps> = (args) => <Checkbox {...args} />;

export const FilterCheckbox = Template.bind({});

FilterCheckbox.args = {
  label: "Active",
};
