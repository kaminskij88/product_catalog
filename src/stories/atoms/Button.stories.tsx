import { Story, Meta } from "@storybook/react";
import Button, { ButtonProps } from "../../app/components/atoms/Button";
import React from "react";

export default {
  title: "Atom/Button",
  component: Button,
  argTypes: {
    onClick: { action: "clicked" },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const ContainedButton = Template.bind({});
export const DisableButton = Template.bind({});

ContainedButton.args = {
  text: "Show Details",
  buttonState: "active",
  variant: "contained",
};

DisableButton.args = {
  text: "Show Details",
  buttonState: "inactive",
  variant: "contained",
};
