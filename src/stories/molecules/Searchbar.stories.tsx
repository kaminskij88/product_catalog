import { Story, Meta } from "@storybook/react";
import Searchbar, {
  SearchbarProps,
} from "../../app/components/molecules/Searchbar";
import React from "react";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import filterReducer from "../../app/store/filterSlice";

const store = configureStore({
  reducer: { filters: filterReducer },
});

export default {
  title: "Molecules/Searchbar",
  component: Searchbar,
} as Meta;

const Template: Story<SearchbarProps> = (args) => (
  <Provider store={store}>
    <Searchbar {...args} />
  </Provider>
);

export const SearchbarExample = Template.bind({});

SearchbarExample.args = {
  placeholder: "Search",
  variant: "outlined",
  fullWidth: true,
};
