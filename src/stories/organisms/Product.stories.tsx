import { Story, Meta } from "@storybook/react";
import Product, { ProductProps } from "../../app/components/organisms/Product";
import React from "react";

export default {
  title: "Organisms/Product",
  component: Product,
  argTypes: {
    onClick: { action: "clicked" },
  },
} as Meta;

const Template: Story<ProductProps> = (args) => <Product {...args} />;

export const ProductCard = Template.bind({});

ProductCard.args = {
  name: "Test Product",
  description: "Test Description",
  rating: 3,
  active: true,
  promo: true,
};
