import React from "react";
import ReactDOM from "react-dom";

import { AppProviders } from "providers/AppProviders";
import { ThemeProvider } from "@material-ui/core";
import { App } from "./app/App";
import Layout from "app/components/organisms/Layout";
import "./index.css";
import theme from "../src/theme/index";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import store from "../src/app/store/store";

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <AppProviders>
      <Provider store={store}>
        <Layout>
          <App />
        </Layout>
      </Provider>
    </AppProviders>
  </ThemeProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
