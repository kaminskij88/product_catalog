const palette = {
  primary: {
    main: "#2140E8",
    light: "#F0F1F5",
    contrastText: "#ffffff",
  },
  secondary: {
    main: "#e6e6e6",
    dark: "#292929",
    light: "#ffffff",
    contrastText: "#292929",
  },
  text: {
    primary: "#1A1B1D",
    secondary: "#9194A5",
    contrastText: "#ffffff",
  },
  info: {
    main: "#4460F7",
  },
};

export default palette;
