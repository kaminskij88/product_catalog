import palette from "./palette";

const typography = {
  fontFamily: "Nunito, sans-serif",
  fontStyle: "normal",

  h1: {
    color: palette.text.primary,
    fontWeight: 600,
    fontSize: "30px",
    letterSpacing: "0.2rem",
  },
  h2: {
    color: palette.text.primary,
    fontWeight: 600,
    fontSize: "1.5rem",
    letterSpacing: "0.2rem",
  },
  h3: {
    color: palette.text.primary,
    fontWeigt: 600,
    fontSize: "18px",
    lineHeight: "16px",
  },
  h5: {
    color: palette.text.contrastText,
    fontSize: "14px",
    fontWeight: 300,
    lineHeight: "16px",
  },
  body1: {
    color: palette.secondary.light,
    fontWeight: 400,
    fontSize: "14px",
  },
  body2: {
    color: palette.text.secondary,
    fontSize: "14px",
    fontWeight: 300,
    lineHeight: "16px",
  },
  subtitle1: {
    color: palette.text.primary,
    fontWeigt: 600,
    fontSize: "14px",
    lineHeight: "16px",
  },
  subtitle2: {
    color: palette.text.secondary,
    fontWeigt: 600,
    fontSize: "14px",
    lineHeight: "16px",
  },
  button: {
    color: palette.info.main,
    fontWeigt: 600,
    fontSize: "14px",
    lineHeight: "16px",
  },
};

export default typography;
