import { ProductProps } from "app/components/organisms/Product";

export const productObject: ProductProps = {
  active: true,
  description:
    "Quam soluta et consequuntur velit ipsa sint facere occaecati fugiat.",
  id: 1,
  image: "https://picsum.photos/640/480?random=4946",
  name: "Awesome Steel Fish",
  promo: true,
  rating: 2,
};

export const inactiveProduct: ProductProps = {
  active: false,
  description:
    "Quam soluta et consequuntur velit ipsa sint facere occaecati fugiat.",
  id: 1,
  image: "https://picsum.photos/640/480?random=4946",
  name: "Awesome Steel Fish",
  promo: true,
  rating: 2,
};
