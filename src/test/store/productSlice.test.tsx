import React from "react";
import productReducer, { setProducts } from "../../app/store/productSlice";
import { productObject } from "../mockData/mockData";

describe("Store:Products", () => {
  test("Store return initial state", () =>
    expect(productReducer(undefined, setProducts([]))).toEqual({
      products: [],
    }));

  test("Should be able to add products to state", () => {
    const previousState: any = { products: [] };

    expect(productReducer(previousState, setProducts([productObject]))).toEqual(
      {
        products: [productObject],
      }
    );
  });
});
