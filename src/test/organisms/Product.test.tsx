import React from "react";
import { render } from "@testing-library/react";
import Product from "../../app/components/organisms/Product";
import { productObject, inactiveProduct } from "../mockData/mockData";

describe("Component:organisms:Product", () => {
  it("Should Render Product Card", () => {
    const { getByTestId, getByText } = render(<Product {...productObject} />);

    getByTestId("product-test");
    getByText("Awesome Steel Fish");
  });

  it("Should Render Product Card as disabled if active flag is false", () => {
    const { getByRole } = render(<Product {...inactiveProduct} />);
    const button = getByRole("button");
    expect(button).toHaveAttribute("disabled");
  });
});
