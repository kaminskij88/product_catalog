import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ProductList from "../../app/components/organisms/ProductList";
import paginationReducer from "../../app/store/paginationSlice";
import productReducer from "../../app/store/productSlice";
import filterReducer from "../../app/store/filterSlice";
import { AnyAction, configureStore, Store } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import * as reactRedux from "react-redux";

describe("Component:organisms:ProductList", () => {
  let store: Store<any, AnyAction>;

  beforeEach(() => {
    store = configureStore({
      reducer: {
        products: productReducer,
        filters: filterReducer,
        pagination: paginationReducer,
      },
    });
  });

  it("Should render Products List", () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <ProductList />
      </Provider>
    );

    getByTestId("products-list");
  });
});
