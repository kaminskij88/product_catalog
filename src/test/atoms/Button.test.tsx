import Button from "../../app/components/atoms/Button";
import { fireEvent, render } from "@testing-library/react";
import React from "react";

describe("Component:Atom:Button", () => {
  test("should render button and handle click function", () => {
    const mockFn = jest.fn();

    const { getByText, getByTestId } = render(
      <Button variant={"contained"} text={"test"} onClick={mockFn}></Button>
    );

    fireEvent.click(getByText("test"));
    expect(mockFn).toHaveBeenCalledTimes(1);
    getByTestId("button-test");
  });

  test("should render inactive button and disable click", () => {
    const mockFn = jest.fn();

    const { getByText, getByTestId } = render(
      <Button
        buttonState={"inactive"}
        variant={"contained"}
        text={"test"}
        onClick={mockFn}
      ></Button>
    );

    fireEvent.click(getByText("test"));
    expect(mockFn).toHaveBeenCalledTimes(0);
    getByTestId("button-test");
  });
});
