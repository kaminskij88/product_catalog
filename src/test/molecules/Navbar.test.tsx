import React from "react";
import Navbar from "../../app/components/molecules/Navbar";
import { render } from "@testing-library/react";
import { AnyAction, configureStore, Store } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import authReducer from "../../app/store/authSlice";
import filterReducer from "../../app/store/filterSlice";
import { BrowserRouter as Router } from "react-router-dom";

describe("Component:molecules:Navbar", () => {
  let store: Store<any, AnyAction>;

  beforeEach(() => {
    store = configureStore({
      reducer: {
        auth: authReducer,
        filters: filterReducer,
      },
    });
  });

  it("Check if navbar render", () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <Router>
          <Navbar />
        </Router>
      </Provider>
    );

    getByTestId("navbar-test");
  });
});
