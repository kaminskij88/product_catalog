import React, { FC } from "react";
import { setCurrentPage } from "app/store/paginationSlice";
import { useSelector, useDispatch } from "react-redux";
import { Typography, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  textButton: {
    border: "none",
    cursor: "pointer",
    padding: "2rem",
    backgroundColor: theme.palette.primary.light,
  },
  button: {
    border: "none",
    cursor: "pointer",
    padding: "0.8rem",
    backgroundColor: theme.palette.primary.light,
  },
  active: {
    color: theme.palette.primary.main,
  },
  inactive: {
    color: theme.palette.primary.light,
  },
}));

const Pagination: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const siblingCount = 1;
  const { totalPages, currentPage } = useSelector(
    (state: any) => state.pagination
  );
  let customRange: Array<any> = [];
  const rightSiblingIndex = Math.min(currentPage + siblingCount, totalPages);
  const firstPageIndex = 1;
  const lastPageIndex = totalPages;
  const pages = [...Array(totalPages).keys()].map((num) => num + 1);

  const range = (start: number, end: number) => {
    let length = end - start + 1;

    return Array.from({ length }, (_, idx) => idx + start);
  };

  customRange = pages;

  if (totalPages > 6) {
    let siblings = 1;
    let showDots = true;
    let leftRange = range(1, 3);
    let rightRange = range(totalPages - 2, totalPages);
    let currentInRight = rightRange.includes(currentPage);
    let nextCurrentInRight = rightRange.includes(rightSiblingIndex);

    if (
      totalPages - 2 - (currentPage + 1) > 1 &&
      !rightRange.includes(currentPage)
    ) {
      showDots = true;
    } else {
      showDots = false;
    }

    customRange = [...leftRange, "DOTS", ...rightRange];

    if (currentPage > 2 && !currentInRight) {
      leftRange = range(currentPage - siblings, currentPage + siblings);
      customRange = [...leftRange, "DOTS", ...rightRange];
    }

    if (currentPage > 2 && !showDots) {
      customRange = [...leftRange, ...rightRange];
    }

    if (nextCurrentInRight && !showDots) {
      if (currentPage === totalPages) {
        rightRange = range(totalPages - 2, totalPages);
        leftRange = [];
      } else {
        rightRange = range(currentPage + siblings, totalPages);
        leftRange = range(currentPage - 1, currentPage);
      }

      customRange = [...leftRange, ...rightRange];
    }
  }

  const handleSelectedPage = (page: number) => {
    dispatch(setCurrentPage(page));
  };

  const handleFirstButton = () => {
    dispatch(setCurrentPage(firstPageIndex));
  };

  const handleLastButton = () => {
    dispatch(setCurrentPage(lastPageIndex));
  };

  return (
    <>
      {totalPages > 1 && (
        <>
          <button
            className={classes.textButton}
            onClick={() => handleFirstButton()}
          >
            <Typography variant={currentPage === 1 ? "subtitle2" : "subtitle1"}>
              {"First"}
            </Typography>
          </button>
          {customRange.map((number) =>
            number === "DOTS" ? (
              "..."
            ) : (
              <button
                className={`${classes.button} ${
                  currentPage === number && classes.active
                }`}
                key={number}
                onClick={() => handleSelectedPage(number)}
              >
                <span>{number}</span>
              </button>
            )
          )}
          <button
            className={classes.textButton}
            onClick={() => handleLastButton()}
          >
            <Typography
              variant={currentPage === totalPages ? "subtitle2" : "subtitle1"}
            >
              {"Last"}
            </Typography>
          </button>
        </>
      )}
    </>
  );
};

export default Pagination;
