import React, { useState } from "react";
import ReactDOM from "react-dom";
import { Backdrop, Grid } from "@material-ui/core";
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) =>
  createStyles({
    backdrop: {
      zIndex: 3,
      backgroundColor: "#1A1B1D",
      width: "100%",
      height: "100%",
    },
    modal: {
      zIndex: 30,
      display: "flex",
      backgroundColor: theme.palette.primary.light,
      animation: `$showModal 200ms ${theme.transitions.easing.easeInOut}`,
      justifyContent: "center",
      position: "fixed",
      top: "25%",
    },
    modalContent: {
      display: "flex",
      backgroundColor: "#1A1B1D",
    },
    "@keyframes showModal": {
      "0%": {
        opacity: 0,
        transform: "translateY(-100%)",
      },
      "100%": {
        opacity: 1,
        transform: "translateY(0)",
      },
    },
  })
);

const ModalOverlay = (props: { children: React.ReactChild }) => {
  const classes = useStyles();

  return (
    <Grid item container xs={10} sm={4} md={5} lg={4} className={classes.modal}>
      {props.children}
    </Grid>
  );
};

const portalElement = document.getElementById("overlayes") as HTMLElement;

const Modal = (props: { children: React.ReactChild; onClose: any }) => {
  const classes = useStyles();

  const [open, setOpen] = useState<boolean>(true);

  const handleClose = () => {
    setOpen(false);
    props.onClose(true);
  };

  return (
    <>
      {ReactDOM.createPortal(
        <Backdrop
          open={open}
          onClick={handleClose}
          className={classes.backdrop}
        />,
        portalElement
      )}
      {ReactDOM.createPortal(
        <ModalOverlay data-testid="modal-test">
          <Grid
            className={classes.modalContent}
            xs={12}
            sm={12}
            md={12}
            lg={12}
            item
          >
            {props.children}
          </Grid>
        </ModalOverlay>,
        portalElement
      )}
    </>
  );
};

export default Modal;
