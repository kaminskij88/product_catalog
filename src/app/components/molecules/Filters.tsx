import React, { FC } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Checkbox from "../atoms/Checkbox";
import { Grid } from "@material-ui/core";

import { toogleActive, tooglePromo } from "app/store/filterSlice";
import { useSelector, useDispatch } from "react-redux";
import { setCurrentPage } from "app/store/paginationSlice";

const useStyles = makeStyles({
  form: {
    display: "flex",
    flexDirection: "row",
  },
  checkboxContainer: {
    display: "flex",
  },
});

const Filters: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { active, promo } = useSelector((state: any) => state.filters);

  const handleActive = () => {
    dispatch(toogleActive());
    dispatch(setCurrentPage(1));
  };

  const handlePromo = () => {
    dispatch(tooglePromo());
    dispatch(setCurrentPage(1));
  };

  return (
    <>
      <form className={classes.form}>
        <Grid container spacing={1}>
          <Grid
            item
            xs={6}
            sm={12}
            md={6}
            lg={6}
            className={classes.checkboxContainer}
          >
            <Checkbox label="Active" onChange={handleActive} value={active} />
          </Grid>
          <Grid
            item
            xs={6}
            sm={12}
            md={6}
            lg={6}
            className={classes.checkboxContainer}
          >
            <Checkbox label="Promo" onChange={handlePromo} value={promo} />
          </Grid>
        </Grid>
      </form>
    </>
  );
};

export default Filters;
