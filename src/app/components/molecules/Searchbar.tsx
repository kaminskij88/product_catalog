import React, { FC, useEffect, useState } from "react";
import { TextField, InputAdornment } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import { useDispatch } from "react-redux";
import { setSearchTerm } from "../../store/filterSlice";
import { setCurrentPage } from "app/store/paginationSlice";

const useStyles = makeStyles({
  searchBar: {
    display: "flex",
  },
  form: {
    display: "flex",
    width: "100%",
    padding: "0.5rem",
  },
  iconButton: {
    padding: 10,
  },
});

export interface SearchbarProps {
  variant: "filled" | "standard" | "outlined";
  placeholder: string;
  fullWidth: boolean;
}

const Searchbar: FC<SearchbarProps> = ({ variant, placeholder, fullWidth }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [inputValue, setInputValue] = useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(setSearchTerm(inputValue));
  };

  const handleSearchTermChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  useEffect(() => {
    const identifier = setTimeout(() => {
      dispatch(setSearchTerm(inputValue));
      dispatch(setCurrentPage(1));
    }, 500);

    return () => {
      clearTimeout(identifier);
    };
  }, [inputValue, dispatch]);

  return (
    <form className={classes.form} onSubmit={(e) => handleSubmit(e)}>
      <TextField
        className={classes.searchBar}
        id="outlined-basic"
        variant={variant}
        fullWidth={fullWidth}
        placeholder={placeholder}
        value={inputValue}
        onChange={handleSearchTermChange}
        InputProps={{
          endAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
      />
    </form>
  );
};

export default Searchbar;
