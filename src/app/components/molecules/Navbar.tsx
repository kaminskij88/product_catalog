import { FC } from "react";
import { Grid, makeStyles, Typography, Box } from "@material-ui/core";
import React from "react";
import Searchbar from "./Searchbar";
import Filters from "./Filters";
import Button from "../atoms/Button";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import Avatar from "../atoms/Avatar";

const useStyles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: theme.palette.secondary.light,
    height: "144px",
    [theme.breakpoints.down("xs")]: {
      height: "100%",
    },
    justifyContent: "flex-start",
    flexGrow: 1,
  },
  title: {
    justifyContent: "center",
    display: "flex",
  },
  logo: {
    color: theme.palette.text.primary,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  searchBar: {
    color: theme.palette.text.primary,
    display: "flex",
    alignItems: "center",
  },
  filters: {
    color: theme.palette.text.primary,
    display: "flex",
    alignItems: "center",
  },
  avatar: {
    color: theme.palette.text.primary,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingRight: "2rem",
  },
}));

const Navbar: FC = () => {
  const classes = useStyles();
  const { isAuthenticated } = useSelector((state: any) => state.auth);

  return (
    <Grid
      container
      direction="row"
      className={classes.appBar}
      data-testid="navbar-test"
    >
      <Box clone order={{ xs: 1, sm: 1, md: 1, lg: 1 }}>
        <Grid item xs={6} sm={3} md={3} lg={3} className={classes.logo}>
          <Typography variant="h6">join.tsh.io</Typography>
        </Grid>
      </Box>
      <Box clone order={{ xs: 3, sm: 2, md: 2, lg: 2 }}>
        <Grid item xs={12} sm={5} md={5} lg={3} className={classes.searchBar}>
          <Searchbar
            placeholder={"Search"}
            variant={"outlined"}
            fullWidth={true}
          />
        </Grid>
      </Box>
      <Box clone order={{ xs: 4, sm: 3, md: 3, lg: 3 }}>
        <Grid item xs={12} sm={2} md={2} lg={4} className={classes.filters}>
          <Filters />
        </Grid>
      </Box>
      <Box clone order={{ xs: 2, sm: 4, md: 4, lg: 4 }}>
        <Grid item xs={6} sm={2} md={2} lg={1} className={classes.avatar}>
          {isAuthenticated || localStorage.getItem("auth") ? (
            <Avatar />
          ) : (
            <Button
              variant={"outlined"}
              text={"Login"}
              component={Link}
              to="/login"
            />
          )}
        </Grid>
      </Box>
    </Grid>
  );
};

export default Navbar;
