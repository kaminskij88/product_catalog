import React, { FC } from "react";
import { makeStyles, Grid, Typography } from "@material-ui/core";
import AssignmentLateOutlinedIcon from "@material-ui/icons/AssignmentLateOutlined";

const useStyles = makeStyles((theme) => ({
  emptyCard: {
    backgroundColor: theme.palette.secondary.light,
    display: "flex",
    width: "600px",
    height: "344px",
    marginTop: "10%",
    justifyContent: "center",
    borderRadius: "0.3rem",
    flexDirection: "column",
    alignItems: "center",
  },
  emptyIcon: {
    fontSize: "5rem",
    color: "#B9BDCF",
  },
}));
const EmptyList: FC = () => {
  const classes = useStyles();
  return (
    <Grid
      xs={9}
      sm={10}
      md={10}
      lg={10}
      container
      spacing={1}
      item
      className={classes.emptyCard}
    >
      <Grid item>
        <AssignmentLateOutlinedIcon className={classes.emptyIcon} />
      </Grid>
      <Grid item>
        <Typography variant="h3">{"Ooops… It’s empty here"}</Typography>
      </Grid>
      <Grid item>
        <Typography variant="body2">
          {"There are no products on the list"}
        </Typography>
      </Grid>
    </Grid>
  );
};
export default EmptyList;
