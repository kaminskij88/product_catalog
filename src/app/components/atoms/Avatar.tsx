import React, { FC, useState } from "react";
import {
  Avatar as MaterialAvatar,
  makeStyles,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from "@material-ui/core";
import avatarIamge from "../../../assets/avatar.png";
import { setLogOut } from "app/store/authSlice";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";

const useStyles = makeStyles((theme) => ({
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  menu: {
    marginRight: theme.spacing(2),
  },
}));

const Avatar: FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.removeItem("auth");
    dispatch(setLogOut());
    history.push("/login");
    window.location.reload();
  };

  return (
    <>
      <IconButton onClick={handleClick}>
        <MaterialAvatar
          alt="avatar"
          src={avatarIamge}
          className={classes.large}
        />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        className={classes.menu}
      >
        <MenuItem onClick={handleLogout}>
          <Typography variant="subtitle1">{"Logout"}</Typography>
        </MenuItem>
      </Menu>
    </>
  );
};

export default Avatar;
