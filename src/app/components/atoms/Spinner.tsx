import React, { FC } from "react";
import { makeStyles, CircularProgress, Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  spinner: {
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    height: "50vh",
  },
  top: {
    color: theme.palette.primary.main,
    animationDuration: "550ms",
  },
  circle: {
    strokeLinecap: "round",
  },
}));

export interface SpinnerProps {
  size: number;
}

const Spinner: FC<SpinnerProps> = ({ size }) => {
  const classes = useStyles();

  return (
    <Grid container className={classes.spinner}>
      <CircularProgress
        variant="indeterminate"
        size={size}
        thickness={4}
        className={classes.top}
        classes={{
          circle: classes.circle,
        }}
      />
    </Grid>
  );
};

export default Spinner;
