import React, { FC } from "react";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  checkbox: {
    display: "flex",
    width: "24px",
    height: "24px",
    color: "red",
    cursor: "pointer",
  },
  container: {
    justifyContent: "right",
    display: "flex",
    alignItems: "center",
  },
  label: {
    padding: "0.4rem",
  },
});

export interface CheckboxProps {
  label: string;
  onChange: any;
  value: boolean;
}

const Checkbox: FC<CheckboxProps> = ({ label, onChange, value }) => {
  const classes = useStyles();

  return (
    <label className={classes.container}>
      <input
        type="checkbox"
        checked={value}
        className={classes.checkbox}
        onChange={onChange}
      ></input>
      <Typography variant="subtitle1" className={classes.label}>
        {label}
      </Typography>
    </label>
  );
};

export default Checkbox;
