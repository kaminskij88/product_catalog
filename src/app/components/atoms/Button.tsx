import React, { FC } from "react";
import { Button as MaterialButton, Typography, Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

export interface ButtonProps {
  text: string;
  onClick?: any;
  buttonState?: string;
  alternativeText?: string;
  variant: "contained" | "outlined";
  href?: string;
  component?: any;
  to?: any;
  fullWidth?: boolean;
}

export interface ButtonStyleProps {
  buttonState?: string;
}

const useStyles = makeStyles<Theme, ButtonStyleProps>((theme) => ({
  contained: {
    display: "flex",
    justifyContent: "center",
    flexGrow: 1,
    backgroundColor: (props) =>
      props.buttonState === "active" ? "#4460F7" : "#9194A5",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#2140E8",
    },
  },
  outlined: {
    color: theme.palette.info.main,
    textTransform: "none",
  },
}));

const Button: FC<ButtonProps> = (props) => {
  const classes = useStyles(props);

  return (
    <MaterialButton
      className={`${
        props.variant === "contained" ? classes.contained : classes.outlined
      }`}
      size="medium"
      variant={props.variant}
      onClick={props.onClick}
      color={"primary"}
      component={props.component}
      to={props.to}
      fullWidth={props.fullWidth}
      data-testid={`button-${props.text}`}
      disabled={props.buttonState === "inactive" && true}
    >
      <Typography variant={props.variant === "contained" ? "body1" : "button"}>
        {props.buttonState === "inactive" ? props.alternativeText : props.text}
      </Typography>
    </MaterialButton>
  );
};

export default Button;
