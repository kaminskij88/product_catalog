import React, { useEffect, useState, useCallback, useMemo, FC } from "react";
import { Grid, makeStyles } from "@material-ui/core";
import Product from "../organisms/Product";
import { ProductProps } from "../organisms/Product";
import { useDispatch, useSelector } from "react-redux";
import { setProducts } from "app/store/productSlice";
import { getProducts } from "app/api/productApi";
import { setTotalPages, setItemPerPage } from "app/store/paginationSlice";
import { getSearchParams } from "app/utiles/searchParams";
import Spinner from "../atoms/Spinner";
import EmptyList from "../molecules/EmptyList";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
  },
  productsContainer: {
    padding: "3rem",
    justifyItems: "center",
    backgroundColor: theme.palette.primary.light,
    marginTop: "0.5rem",
  },
  product: {
    display: "flex",
    justifyContent: "center",
  },
}));

const ProductList: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { products } = useSelector((state: any) => state.products);
  const { searchTerm, active, promo } = useSelector(
    (state: any) => state.filters
  );
  const { currentPage } = useSelector((state: any) => state.pagination);

  const [isLoading, setIsLoading] = useState(false);
  const [emptyProducts, setEmptyProducts] = useState(false);

  const searchFilters = useMemo(
    () => getSearchParams(searchTerm, promo, active, currentPage),
    [searchTerm, promo, active, currentPage]
  );

  const fetchProducts = useCallback(() => {
    setIsLoading(true);
    getProducts(searchFilters)
      .then((result) => {
        const fetchedProducts = result.data.items;
        const meta = result.data.meta;
        dispatch(setTotalPages(meta.totalPages));
        dispatch(setItemPerPage(meta.itemsPerPage));
        setEmptyProducts(false);

        if (!Object.keys(fetchedProducts).length) {
          setEmptyProducts(true);
        }

        dispatch(setProducts(fetchedProducts));
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [dispatch, searchFilters]);

  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);

  return (
    <Grid container className={classes.root} data-testid="products-list">
      {isLoading ? (
        <Spinner size={40} />
      ) : !emptyProducts ? (
        <Grid container className={classes.productsContainer} spacing={4}>
          {products &&
            products.map((product: ProductProps) => {
              return (
                <Grid
                  key={product.id}
                  className={classes.product}
                  item
                  xs={12}
                  sm={5}
                  md={4}
                  lg={3}
                >
                  <Product {...product} />
                </Grid>
              );
            })}
        </Grid>
      ) : (
        <EmptyList />
      )}
    </Grid>
  );
};

export default ProductList;
