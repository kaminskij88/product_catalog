import React from "react";
import { FC } from "react";
import Navbar from "../molecules/Navbar";
import { useLocation } from "react-router";

const Layout: FC = (props) => {
  const { pathname } = useLocation();

  return (
    <>
      {pathname !== "/login" && <Navbar />}
      {props.children}
    </>
  );
};

export default Layout;
