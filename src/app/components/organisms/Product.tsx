import React, { FC, useState } from "react";
import {
  Card,
  CardMedia,
  Typography,
  CardContent,
  CardActions,
  makeStyles,
} from "@material-ui/core";
import { Rating } from "@material-ui/lab";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import ProductDetails from "./ProductDetails";
import Button from "../atoms/Button";
import Modal from "../molecules/Modal";

export interface ProductProps {
  id: number;
  name: string;
  image: string;
  description: string;
  rating: number;
  promo: boolean;
  active: boolean;
}

const useStyles = makeStyles((theme) => ({
  media: {
    width: "100%",
    height: "40%",
  },
  card: {
    width: "288px",
    height: "400px",
    display: "flex",
    flexDirection: "column",
    position: "relative",
  },
  cardContent: {
    height: "30%",
  },
  detailButton: {
    display: "flex",
    justifyContent: "center",
    flexGrow: 1,
    alignItems: "flex-end",
    marginBottom: "3px",
  },
  raiting: {
    display: "flex",
    justifyContent: "flex-start",
  },
  promo: {
    backgroundColor: "#F9A52B",
    display: "flex",
    width: "75px",
    height: "24px",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "29px",
  },
  overlay: {
    backgroundColor: theme.palette.primary.light,
    zIndex: 2,
    height: "100%",
    width: "100%",
    opacity: 0.5,
    position: "absolute",
  },
}));

const Product: FC<ProductProps> = (product) => {
  const classes = useStyles();
  const [openModal, setOpenModal] = useState<boolean>(false);

  const showProductDetails = () => {
    setOpenModal(true);
  };

  const hideProductDetails = () => {
    setOpenModal(false);
  };

  return (
    <>
      <Card className={classes.card} data-testid="product-test">
        <CardMedia
          className={classes.media}
          image={product.image}
          title={product.name}
        >
          {!product.active && <div className={classes.overlay}></div>}
          {product.promo && (
            <div className={classes.promo}>
              <Typography component="p" variant="h5">
                {"Promo"}
              </Typography>
            </div>
          )}
        </CardMedia>
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h3" component="h2">
            {product.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {product.description}
          </Typography>
        </CardContent>
        <CardContent className={classes.raiting}>
          <Rating
            name="read-only"
            value={product.rating}
            readOnly
            emptyIcon={<StarBorderIcon fontSize="inherit" />}
          />
        </CardContent>
        <CardActions className={classes.detailButton}>
          <Button
            buttonState={product.active ? "active" : "inactive"}
            text={"Show Details"}
            alternativeText={"Unavailable"}
            onClick={showProductDetails}
            variant={"contained"}
            data-testid="test-show-details"
          />
        </CardActions>
      </Card>
      {openModal && (
        <Modal onClose={hideProductDetails}>
          <ProductDetails
            image={product.image}
            name={product.name}
            description={product.description}
            closeDetails={hideProductDetails}
          />
        </Modal>
      )}
    </>
  );
};

export default Product;
