import React, { FC } from "react";
import {
  Card,
  CardMedia,
  IconButton,
  Typography,
  CardContent,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
  media: {
    width: "100%",
    height: "203px",
    display: "flex",
    flexDirection: "column",
  },
  card: {
    width: "100%",
    height: "330px",
    display: "flex",
    flexDirection: "column",
    borderRadius: "10px",
  },
  close: {
    justifyContent: "flex-end",
    padding: 0,
    color: theme.palette.text.primary,
    alignItems: "baseline",
    "&:hover": {
      backgroundColor: "unset",
    },
  },
}));

export interface ProductDetailsProps {
  image: string;
  name: string;
  description: string;
  closeDetails?: any;
}

const ProductDetails: FC<ProductDetailsProps> = ({
  image,
  name,
  description,
  closeDetails,
}) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardMedia className={classes.media} image={image} title={name}>
        <IconButton
          aria-label="settings"
          disableRipple={true}
          disableFocusRipple={true}
          className={classes.close}
          onClick={closeDetails}
        >
          <CloseIcon />
        </IconButton>
      </CardMedia>
      <CardContent>
        <Typography gutterBottom variant="h3" component="h2">
          {name}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {description}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default ProductDetails;
