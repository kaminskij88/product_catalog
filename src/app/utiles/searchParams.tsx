export const getSearchParams = (
  searchTerm?: string,
  promo?: string,
  active?: string,
  currentPage?: string
): object => {
  return {
    limit: 8,
    ...(searchTerm && { search: searchTerm }),
    ...(promo && { promo: promo }),
    ...(active && { active: active }),
    ...(currentPage && { page: currentPage }),
  };
};
