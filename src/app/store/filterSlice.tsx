import { createSlice } from "@reduxjs/toolkit";

interface SliceState {
  searchTerm: string;
  active: boolean;
  promo: boolean;
}

const initialFilterState: SliceState = {
  searchTerm: "",
  active: false,
  promo: false,
};
const filterSlice = createSlice({
  name: "filters",
  initialState: initialFilterState,
  reducers: {
    setSearchTerm(state, { payload }) {
      state.searchTerm = payload;
    },
    toogleActive(state) {
      state.active = !state.active;
    },
    tooglePromo(state) {
      state.promo = !state.promo;
    },
  },
});

export const { setSearchTerm, toogleActive, tooglePromo } = filterSlice.actions;

export default filterSlice.reducer;
