import { createSlice } from "@reduxjs/toolkit";

interface SliceState {
  products: Array<any>;
}

const initialProductState: SliceState = { products: [] };

const productSlice = createSlice({
  name: "products",
  initialState: initialProductState,
  reducers: {
    setProducts(state, { payload }) {
      state.products = payload;
    },
  },
});

export const { setProducts } = productSlice.actions;

export default productSlice.reducer;
