import { configureStore } from "@reduxjs/toolkit";
import productReducer from "./productSlice";
import filterReducer from "./filterSlice";
import paginationReducer from "./paginationSlice";
import authReducer from "./authSlice";

const store = configureStore({
  reducer: {
    products: productReducer,
    filters: filterReducer,
    pagination: paginationReducer,
    auth: authReducer,
  },
});

export default store;
