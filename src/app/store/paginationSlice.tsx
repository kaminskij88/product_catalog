import { createSlice } from "@reduxjs/toolkit";

interface SliceState {
  totalItems: number;
  itemCount: number;
  itemPerPage: number;
  totalPages: number;
  currentPage: number;
}

const initialPaginationState: SliceState = {
  totalItems: 0,
  itemCount: 0,
  itemPerPage: 0,
  totalPages: 0,
  currentPage: 1,
};
const paginationSlice = createSlice({
  name: "pagination",
  initialState: initialPaginationState,
  reducers: {
    setCurrentPage(state, { payload }) {
      state.currentPage = payload;
    },
    setTotalItem(state, { payload }) {
      state.totalItems = payload;
    },
    setItemCount(state, { payload }) {
      state.itemCount = payload;
    },
    setItemPerPage(state, { payload }) {
      state.itemPerPage = payload;
    },
    setTotalPages(state, { payload }) {
      state.totalPages = payload;
    },
  },
});

export const {
  setCurrentPage,
  setTotalPages,
  setTotalItem,
  setItemCount,
  setItemPerPage,
} = paginationSlice.actions;

export default paginationSlice.reducer;
