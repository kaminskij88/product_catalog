import { createSlice } from "@reduxjs/toolkit";

interface SliceState {
  isAuthenticated: boolean;
}

const initialProductState: SliceState = { isAuthenticated: false };

const authSlice = createSlice({
  name: "auth",
  initialState: initialProductState,
  reducers: {
    setIsAuthenticated(state) {
      state.isAuthenticated = true;
    },
    setLogOut(state) {
      state.isAuthenticated = false;
    },
  },
});

export const { setIsAuthenticated, setLogOut } = authSlice.actions;

export default authSlice.reducer;
