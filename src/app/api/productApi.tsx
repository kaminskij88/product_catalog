import axiosInstance from "./axios";

export type Products = {
  items: object;
  meta: { totalPages: number; itemsPerPage: number };
};

export const getProducts = (params: object) => {
  return axiosInstance.get<Products>("/products", { params });
};
