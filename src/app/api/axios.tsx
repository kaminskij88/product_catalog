import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://join-tsh-api-staging.herokuapp.com/",
  headers: {
    Accept: "application/json",
  },
});

axiosInstance.interceptors.request.use(
  (request) => {
    request.headers["Content-Type"] = "application/json";
    return request;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosInstance;
