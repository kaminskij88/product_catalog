import React from "react";
import { render } from "test";
import { Products } from "./Products";
import paginationReducer from "../store/paginationSlice";
import productReducer from "../store/productSlice";
import filterReducer from "../store/filterSlice";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

describe("Products", () => {
  test("Displays page header", async () => {
    const store = configureStore({
      reducer: {
        pagination: paginationReducer,
        products: productReducer,
        filters: filterReducer,
      },
    });
    const { getByTestId } = render(
      <Provider store={store}>
        <Products />
      </Provider>
    );

    getByTestId("products");
  });
});
