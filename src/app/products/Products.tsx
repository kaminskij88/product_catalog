import React, { FC } from "react";
import { makeStyles, Grid } from "@material-ui/core";
import ProductList from "../components/organisms/ProductList";

import Pagination from "../components/molecules/Pagination";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary.light,
    display: "flex",
    justifyContent: "center",
    height: "100vh",
  },
  check: {
    height: "100vh",
  },
  paginationContainer: {
    display: "flex",
    height: "2rem",
    justifyContent: "center",
    alignItems: "center",
  },
}));

export const Products: FC = () => {
  const classes = useStyles();

  return (
    <Grid container className={classes.root} data-testid="products">
      <>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <ProductList />
        </Grid>
        <Grid
          className={classes.paginationContainer}
          item
          xs={12}
          sm={12}
          md={12}
          lg={12}
        >
          <Pagination />
        </Grid>
      </>
    </Grid>
  );
};
