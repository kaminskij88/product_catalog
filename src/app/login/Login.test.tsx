import React from "react";
import { render } from "test";
import { Login } from "./Login";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../store/authSlice";

describe("Login", () => {
  test("Displays all information", async () => {
    const store = configureStore({
      reducer: { auth: authReducer },
    });

    const { getByText, getByLabelText } = render(
      <Provider store={store}>
        <Login />
      </Provider>
    );

    expect(getByText("Login")).toBeInTheDocument();
  });
});
