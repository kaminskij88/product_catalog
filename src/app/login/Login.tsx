import React, { FC } from "react";
import { Link } from "react-router-dom";
import { Grid } from "@material-ui/core";
import { useDispatch } from "react-redux";
import {
  makeStyles,
  Hidden,
  Typography,
  TextField,
  InputLabel,
} from "@material-ui/core";
import loginImage from "../../assets/login_image.jpeg";
import Button from "app/components/atoms/Button";
import { setIsAuthenticated } from "app/store/authSlice";

const useStyles = makeStyles((theme) => ({
  loginContainer: {
    display: "flex",
    height: "100vh",
    width: "100%",
    backgroundColor: "#ffffff",
    flexDirection: "row",
  },
  imageContainer: {
    display: "flex",
    width: "100%",
    backgroundImage: `url(${loginImage})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  },
  formContainer: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    padding: "0.5rem 7rem 0 7rem",
    [theme.breakpoints.down("xs")]: {
      padding: "0.5rem 2rem 0 2rem",
    },
    flexGrow: 1,
  },
  companyText: {
    paddingTop: "2rem",
    paddingBottom: "9rem",
  },
}));

export const Login: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleLogin = () => {
    dispatch(setIsAuthenticated());
    localStorage.setItem("auth", "1");
  };

  return (
    <>
      <Grid
        className={classes.loginContainer}
        container
        item
        xs={12}
        sm={12}
        md={12}
        lg={12}
      >
        <Hidden only={["xs"]}>
          <Grid
            item
            sm={6}
            md={6}
            lg={6}
            className={classes.imageContainer}
          ></Grid>
        </Hidden>
        <Grid
          container
          item
          xs={12}
          sm={6}
          md={6}
          lg={6}
          className={classes.formContainer}
        >
          <Grid
            xs={12}
            sm={12}
            md={12}
            lg={12}
            item
            className={classes.companyText}
          >
            <Typography variant="h6">join.tsh.io</Typography>
          </Grid>
          <Grid xs={12} sm={12} md={12} lg={12} item>
            <Typography variant="h1">Login</Typography>
          </Grid>
          <Grid xs={12} sm={12} md={12} lg={12} item>
            <InputLabel>
              <Typography variant="subtitle1">{"Username"}</Typography>
            </InputLabel>
            <TextField
              variant="outlined"
              fullWidth={true}
              placeholder={"Enter username"}
            ></TextField>
          </Grid>
          <Grid xs={12} sm={12} md={12} lg={12} item>
            <InputLabel>
              <Typography variant="subtitle1">{"Password"}</Typography>
            </InputLabel>
            <TextField
              variant="outlined"
              fullWidth={true}
              placeholder={"Enter password"}
            ></TextField>
          </Grid>
          <Grid xs={12} sm={12} md={12} lg={12} item>
            <Button
              text={"Log in"}
              variant={"contained"}
              buttonState={"active"}
              fullWidth={true}
              onClick={() => handleLogin()}
              component={Link}
              to={"/products"}
            />
          </Grid>
          <Grid xs={12} sm={12} md={12} lg={12} item>
            <Typography variant="body2">{"Forgot password?"}</Typography>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
